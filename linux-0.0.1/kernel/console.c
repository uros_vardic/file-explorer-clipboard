/*
 *	console.c
 *
 * This module implements the console io functions
 *	'void con_init(void)'
 *	'void con_write(struct tty_queue * queue)'
 * Hopefully this will be a rather complete VT102 implementation.
 *
 */

/*
 *  NOTE!!! We sometimes disable and enable interrupts for a short while
 * (to put a word in video IO), but this will work even for keyboard
 * interrupts. We know interrupts aren't enabled when getting a keyboard
 * interrupt, as we use trap-gates. Hopefully all is well.
 */

#include <linux/sched.h>
#include <linux/tty.h>
#include <asm/io.h>
#include <asm/system.h>

#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>

#define SCREEN_START 0xb8000
#define SCREEN_END   0xc0000
#define LINES 25
#define COLUMNS 80
#define NPAR 16

extern void keyboard_interrupt(void);

static unsigned long origin=SCREEN_START;
static unsigned long scr_end=SCREEN_START+LINES*COLUMNS*2;
static unsigned long pos;
static unsigned long x,y;
static unsigned long top=0,bottom=LINES;
static unsigned long lines=LINES,columns=COLUMNS;
static unsigned long state=0;
static unsigned long npar,par[NPAR];
static unsigned long ques=0;
static unsigned char attr=0x07;

/*
 * this is what the terminal answers to a ESC-Z or csi0c
 * query (= vt100 response).
 */
#define RESPONSE "\033[?1;2c"

static inline void gotoxy(unsigned int new_x,unsigned int new_y)
{
	if (new_x>=columns || new_y>=lines)
		return;
	x=new_x;
	y=new_y;
	pos=origin+((y*columns+x)<<1);
}

static inline void set_origin(void)
{
	cli();
	outb_p(12,0x3d4);
	outb_p(0xff&((origin-SCREEN_START)>>9),0x3d5);
	outb_p(13,0x3d4);
	outb_p(0xff&((origin-SCREEN_START)>>1),0x3d5);
	sti();
}

static void scrup(void)
{
	if (!top && bottom==lines) {
		origin += columns<<1;
		pos += columns<<1;
		scr_end += columns<<1;
		if (scr_end>SCREEN_END) {
			
			int d0,d1,d2,d3;
			__asm__ __volatile("cld\n\t"
				"rep\n\t"
				"movsl\n\t"
				"movl %[columns],%1\n\t"
				"rep\n\t"
				"stosw"
				:"=&a" (d0), "=&c" (d1), "=&D" (d2), "=&S" (d3)
				:"0" (0x0720),
				 "1" ((lines-1)*columns>>1),
				 "2" (SCREEN_START),
				 "3" (origin),
				 [columns] "r" (columns)
				:"memory");

			scr_end -= origin-SCREEN_START;
			pos -= origin-SCREEN_START;
			origin = SCREEN_START;
		} else {
			int d0,d1,d2;
			__asm__ __volatile("cld\n\t"
				"rep\n\t"
				"stosl"
				:"=&a" (d0), "=&c" (d1), "=&D" (d2) 
				:"0" (0x07200720),
				"1" (columns>>1),
				"2" (scr_end-(columns<<1))
				:"memory");
		}
		set_origin();
	} else {
		int d0,d1,d2,d3;
		__asm__ __volatile__("cld\n\t"
			"rep\n\t"
			"movsl\n\t"
			"movl %[columns],%%ecx\n\t"
			"rep\n\t"
			"stosw"
			:"=&a" (d0), "=&c" (d1), "=&D" (d2), "=&S" (d3)
			:"0" (0x0720),
			"1" ((bottom-top-1)*columns>>1),
			"2" (origin+(columns<<1)*top),
			"3" (origin+(columns<<1)*(top+1)),
			[columns] "r" (columns)
			:"memory");
	}
}

static void scrdown(void)
{
	int d0,d1,d2,d3;
	__asm__ __volatile__("std\n\t"
		"rep\n\t"
		"movsl\n\t"
		"addl $2,%%edi\n\t"	/* %edi has been decremented by 4 */
		"movl %[columns],%%ecx\n\t"
		"rep\n\t"
		"stosw"
		:"=&a" (d0), "=&c" (d1), "=&D" (d2), "=&S" (d3)
		:"0" (0x0720),
		"1" ((bottom-top-1)*columns>>1),
		"2" (origin+(columns<<1)*bottom-4),
		"3" (origin+(columns<<1)*(bottom-1)-4),
		[columns] "r" (columns)
		:"memory");
}

static void lf(void)
{
	if (y+1<bottom) {
		y++;
		pos += columns<<1;
		return;
	}
	scrup();
}

static void ri(void)
{
	if (y>top) {
		y--;
		pos -= columns<<1;
		return;
	}
	scrdown();
}

static void cr(void)
{
	pos -= x<<1;
	x=0;
}

static void del(void)
{
	if (x) {
		pos -= 2;
		x--;
		*(unsigned short *)pos = 0x0720;
	}
}

static void csi_J(int par)
{
	long count;
	long start;

	switch (par) {
		case 0:	/* erase from cursor to end of display */
			count = (scr_end-pos)>>1;
			start = pos;
			break;
		case 1:	/* erase from start to cursor */
			count = (pos-origin)>>1;
			start = origin;
			break;
		case 2: /* erase whole display */
			count = columns*lines;
			start = origin;
			break;
		default:
			return;
	}
	int d0,d1,d2;
	__asm__ __volatile__("cld\n\t"
		"rep\n\t"
		"stosw\n\t"
		:"=&c" (d0), "=&D" (d1), "=&a" (d2)
		:"0" (count),"1" (start),"2" (0x0720)
		:"memory");
}

static void csi_K(int par)
{
	long count;
	long start;

	switch (par) {
		case 0:	/* erase from cursor to end of line */
			if (x>=columns)
				return;
			count = columns-x;
			start = pos;
			break;
		case 1:	/* erase from start of line to cursor */
			start = pos - (x<<1);
			count = (x<columns)?x:columns;
			break;
		case 2: /* erase whole line */
			start = pos - (x<<1);
			count = columns;
			break;
		default:
			return;
	}
	int d0,d1,d2;
	__asm__ __volatile__("cld\n\t"
		"rep\n\t"
		"stosw\n\t"
		:"=&c" (d0), "=&D" (d1), "=&a" (d2)
		:"0" (count),"1" (start),"2" (0x0720)
		:"memory");
}

void csi_m(void)
{
	int i;

	for (i=0;i<=npar;i++)
		switch (par[i]) {
			case 0:attr=0x07;break;
			case 1:attr=0x0f;break;
			case 4:attr=0x0f;break;
			case 7:attr=0x70;break;
			case 27:attr=0x07;break;
		}
}

static inline void set_cursor(void)
{
	cli();
	outb_p(14,0x3d4);
	outb_p(0xff&((pos-SCREEN_START)>>9),0x3d5);
	outb_p(15,0x3d4);
	outb_p(0xff&((pos-SCREEN_START)>>1),0x3d5);
	sti();
}

static void respond(struct tty_struct * tty)
{
	char * p = RESPONSE;

	cli();
	while (*p) {
		PUTCH(*p,tty->read_q);
		p++;
	}
	sti();
	copy_to_cooked(tty);
}

static void insert_char(void)
{
	int i=x;
	unsigned short tmp,old=0x0720;
	unsigned short * p = (unsigned short *) pos;

	while (i++<columns) {
		tmp=*p;
		*p=old;
		old=tmp;
		p++;
	}
}

static void insert_line(void)
{
	int oldtop,oldbottom;

	oldtop=top;
	oldbottom=bottom;
	top=y;
	bottom=lines;
	scrdown();
	top=oldtop;
	bottom=oldbottom;
}

static void delete_char(void)
{
	int i;
	unsigned short * p = (unsigned short *) pos;

	if (x>=columns)
		return;
	i = x;
	while (++i < columns) {
		*p = *(p+1);
		p++;
	}
	*p=0x0720;
}

static void delete_line(void)
{
	int oldtop,oldbottom;

	oldtop=top;
	oldbottom=bottom;
	top=y;
	bottom=lines;
	scrup();
	top=oldtop;
	bottom=oldbottom;
}

static void csi_at(int nr)
{
	if (nr>columns)
		nr=columns;
	else if (!nr)
		nr=1;
	while (nr--)
		insert_char();
}

static void csi_L(int nr)
{
	if (nr>lines)
		nr=lines;
	else if (!nr)
		nr=1;
	while (nr--)
		insert_line();
}

static void csi_P(int nr)
{
	if (nr>columns)
		nr=columns;
	else if (!nr)
		nr=1;
	while (nr--)
		delete_char();
}

static void csi_M(int nr)
{
	if (nr>lines)
		nr=lines;
	else if (!nr)
		nr=1;
	while (nr--)
		delete_line();
}

static int saved_x=0;
static int saved_y=0;

static void save_cur(void)
{
	saved_x=x;
	saved_y=y;
}

static void restore_cur(void)
{
	x=saved_x;
	y=saved_y;
	pos=origin+((y*columns+x)<<1);
}

//pored ove implementacije, imamo i deklaraciju funkcije u include/linux/tty.h
void prikazi_root_velicinu(void)
{
	struct m_inode *dir_inode;
	struct m_inode *root_inode;
	short* my_vmem_pos;
	char* file_name = "/root"; //ime direktorijuma za koji dohvatamo velicinu
	int xPos = 50;
	int yPos = 6;

	//da bi namei funkcija mogla da obilazi FS, moramo da imamo podesen
	//pokazivac na root inode i na pwd inode
	//posto se ova funkcija poziva unutar kernel-a, oba ova ce biti NULL
	//tako da namei ne bi funkcionisao bez naredne tri linije
	//stvar je u tome da sistem nije dizajniran tako da se unutar kernela radi sa fajlovima
	//mi ovo radimo samo da bismo povezali razlicite komponente u kernelu kao skolski primer
	root_inode = iget(0x301, 1); //dohvatamo prvi inode na prvom disku
	current->root = root_inode;
	current->pwd = root_inode;

	//namei funkcija nam dohvata inode na osnovu putanje
	dir_inode = namei(file_name);
	int size = dir_inode->i_size; //vrednost koju prikazujemo

	my_vmem_pos = origin + COLUMNS * 2 * yPos + xPos * 2; //nasa pozicija u video memoriji
	while (size > 0) {
		int digit = size % 10; //sracunamo najdesniju cifru
		char char_digit = digit + '0';
		*my_vmem_pos = ((short)0x02 << 8) | char_digit; //prikazemo
		my_vmem_pos--; //posto je pokazivac na short, ovo nas vraca dva bajta na levo
		size /= 10; //sledeca cifra
	}

	//svaki iget mora da ima svoj iput, slicno kao sto svaki open mora da ima svoj close
	//iget zapravo belezi dohvaceni inode u internom nizu
	//ako zanemarimo iput, taj niz ce se pre ili kasnije prepuniti
	iput(root_inode);
	//namei je unutar sebe radio iget, tako da i za ovo treba iput
	iput(dir_inode);
	current->root = NULL;
	current->pwd = NULL;
	
}

int is_title_pos(int title_length, int x_pos, int y_pos) {
	int title_pos = (22 - title_length) / 2 + 58;

	if (y_pos == 0 && x_pos == title_pos - 2)
		return 1;

	return 0;
}

int print_title(char* title, int title_length, int x_pos) {
	int i;

	short* screen_pos;

	// title_length + 4 for '[  ]' in title
	for (i = 0; i < title_length + 4; i++) {
		// y_pos is always 0
		screen_pos = origin + COLUMNS * 2 * 0 + x_pos * 2;

		if (i == 0)
			*screen_pos = ((short) attr << 8) | '[';

		else if (i == 1) 
			*screen_pos = ((short) attr << 8) | ' ';

		else if (i == title_length + 2)
			*screen_pos = ((short) attr << 8) | ' ';

		else if (i == title_length + 3)
			*screen_pos = ((short) attr << 8) | ']';

		else
			*screen_pos = ((short) attr << 8) | title[i - 2];
		
		x_pos++;
		screen_pos--;
	}

	// returns how much we moved 
	return i;
}

int is_border(int x_pos, int y_pos) {
	if (x_pos == 58 || x_pos == 79 || y_pos == 0 || y_pos == 11)
		return 1;

	return 0;
}

int is_entry_pos(int entry_length, int x_pos) {
	int entry_pos = (22 - entry_length) / 2 + 58;

	if (entry_pos == x_pos)
		return 1;

	return 0;
}

int print_entry_name(char* entry_name, int entry_length, int x_pos, int y_pos, int text_color) {
	int i;

	short* screen_pos;

	for (i = 0; i < entry_length; i++) {
		screen_pos = origin + COLUMNS * 2 * y_pos + x_pos * 2;

		*screen_pos = ((short) text_color << 8) | entry_name[i];

		screen_pos--;
		x_pos++;
	}

	// returns how much we moved
	return i;
}

void reset_char_array(char* array, int array_length) {
	int i = 0;

	for (i = 0; i < array_length; i++) 
		array[i] = '\0';
}

void append(char* array, char* to_append) {
	int i = 0, j = 0;

	while (array[j] != '\0')
		j++;

	while (to_append[i] != '\0') {
		array[j++] = to_append[i];
		i++;
	}

	array[j] = '\0';
}

int get_text_color(struct m_inode *dir_inode) {
	if (S_ISCHR(dir_inode->i_mode))
		return 0x06;
	if (S_ISDIR(dir_inode->i_mode))
		return 0x03;
	if (dir_inode->i_mode & S_IXUSR)
		return 0x02;
	if (S_ISREG(dir_inode->i_mode))
		return 0x07;

	return 0x04;
}

int get_selected_text_color(struct m_inode *dir_inode) {
	if (S_ISCHR(dir_inode->i_mode))
		return 0x86;
	if (S_ISDIR(dir_inode->i_mode))
		return 0x83;
	if (dir_inode->i_mode & S_IXUSR)
		return 0x82;
	if (S_ISREG(dir_inode->i_mode))
		return 0x87;

	return 0x84;
}

void write_selected_file(char* new_path, char* old_path, int old_path_length, char* enty_name, int length) {
	reset_char_array(new_path, 512);

	int i, j;

	for (i = 0; i < old_path_length; i++)
		new_path[i] = old_path[i];

	for (j = 0; j < length; j++)
		new_path[i++] = enty_name[j];

	new_path[i] = '/';
}

char* file_app(char* path, int path_length, int selected) {
	int x_pos = 58, y_pos = 0, i, entry_length;

	short* screen_pos;

	char namei_path[512], new_path[512];

	struct dirent entry;
	struct m_inode *dir_inode;
	struct m_inode *root_inode;	

	root_inode = iget(0x301, 1);
	current->root = root_inode;
	current->pwd = root_inode;

	int current_dir_fd = open(path, O_RDONLY);

	while (y_pos < 12) {	
		entry_length = getdents(current_dir_fd, &entry, 1);

		if (entry.d_name[0] == '.') 
			entry_length = getdents(current_dir_fd, &entry, 1);
			// continue;

		append(namei_path, path);
		append(namei_path, entry.d_name);
		dir_inode = namei(namei_path);

		reset_char_array(namei_path, 512);

		// 80 - 58 = 22 chars per row
		while (x_pos < 80) {
			screen_pos = origin + COLUMNS * 2 * y_pos + x_pos * 2;

			// title state
			if (is_title_pos(path_length, x_pos, y_pos) == 1) {
				x_pos += print_title(path, path_length, x_pos);
				continue;
			}

			// border state
			else if (is_border(x_pos, y_pos))
				*screen_pos = ((short) attr << 8) | '#';

			// content state
			else {
				if (entry_length != 0 && is_entry_pos(entry_length, x_pos)) {
					int text_color;

					// row is selected
					if (y_pos == selected + 1) {
						text_color = get_selected_text_color(dir_inode);
						write_selected_file(new_path, path, path_length, entry.d_name, entry_length);
					} else
						text_color = get_text_color(dir_inode);

					x_pos += print_entry_name(entry.d_name, entry_length, x_pos, y_pos, text_color);
					continue;
				}

				*screen_pos = ((short) attr << 8) | ' ';
			}

			x_pos++;
			screen_pos--;
		}

		y_pos++;
		x_pos = 58;
	}

	iput(root_inode);
	iput(dir_inode);
	current->root = NULL;
	current->pwd = NULL;
	close(current_dir_fd);

	return new_path;
}

int get_text_size(char text[20]) {
	int i = 0;
	while (text[i] != '\0')
		i++;

	return i;
}

int print_text(char texts[10][20], int x_pos, int y_pos, int color) {
	int i = 0, text_size = get_text_size(texts[y_pos - 1]);

	short* screen_pos;

	if (is_entry_pos(text_size, x_pos) == 0)
		return 0;

	for (i = 0; i < text_size; i++) {
		screen_pos = origin + COLUMNS * 2 * y_pos + x_pos * 2;
		*screen_pos = ((short) color << 8) | texts[y_pos - 1][i];
		x_pos++;
		screen_pos--;
	}

	return i;
}

char* clipboard_app(char texts[10][20], int selected) {
	int x_pos = 58, y_pos = 0, i, color = 0x07, write_flag = 0;

	short* screen_pos;

	char copy[20];

	while (y_pos < 12) {
		while (x_pos < 80) {
			screen_pos = origin + COLUMNS * 2 * y_pos + x_pos * 2;
			
			if (y_pos == 0 && x_pos == 63) {
				x_pos += print_title("clipboard", 9, x_pos);
				continue;
			} else if (is_border(x_pos, y_pos)) {
				*screen_pos = ((short) attr << 8) | '#';
			} else {
				if (y_pos == selected + 1) {
					color = 0x87;

					i = 0;
					while (texts[y_pos - 1][i] != '\0') {
						copy[i] = texts[y_pos - 1][i];
						i++;
					}

					copy[i] = '\0'; 
				} else {
					color = 0x07;
				}
				
				x_pos += print_text(texts, x_pos, y_pos, color);

				screen_pos = origin + COLUMNS * 2 * y_pos + x_pos * 2;
				*screen_pos = ((short) color << 8) | ' ';
			}

			screen_pos--;
			x_pos++;
		}

		y_pos++;
		x_pos = 58;
	}


	return copy;
}

void clear_error() {
	int x_pos = 58, y_pos = 13;

	short* screen_pos;

	while (x_pos != 80) {
		screen_pos = origin + COLUMNS * 2 * y_pos + x_pos * 2;
		*screen_pos = ((short) 0x07 << 8) | ' ';
		screen_pos--;
		x_pos++;
	}	
}

void print_error(char* message) {
	int x_pos = 58, y_pos = 13, i = 0;

	short* screen_pos;

	clear_error();

	while (message[i] != '\0') {
		screen_pos = origin + COLUMNS * 2 * y_pos + x_pos * 2;
		*screen_pos = ((short) 0x04 << 8) | message[i++];
		screen_pos--;
		x_pos++;
	}
}

void con_write(struct tty_struct * tty)
{
	int nr;
	char c;

	nr = CHARS(tty->write_q);
	while (nr--) {
		GETCH(tty->write_q,c);
		switch(state) {
			case 0:
				if (c>31 && c<127) {
					if (x>=columns) {
						x -= columns;
						pos -= columns<<1;
						lf();
					}
					__asm__("movb attr,%%ah\n\t"
						"movw %%ax,%1\n\t"
						::"a" (c),"m" (*(short *)pos)
						/*:"ax"*/);
					pos += 2;
					x++;
				} else if (c==27)
					state=1;
				else if (c==10 || c==11 || c==12)
					lf();
				else if (c==13)
					cr();
				else if (c==ERASE_CHAR(tty))
					del();
				else if (c==8) {
					if (x) {
						x--;
						pos -= 2;
					}
				} else if (c==9) {
					c=8-(x&7);
					x += c;
					pos += c<<1;
					if (x>columns) {
						x -= columns;
						pos -= columns<<1;
						lf();
					}
					c=9;
				}
				break;
			case 1:
				state=0;
				if (c=='[')
					state=2;
				else if (c=='E')
					gotoxy(0,y+1);
				else if (c=='M')
					ri();
				else if (c=='D')
					lf();
				else if (c=='Z')
					respond(tty);
				else if (x=='7')
					save_cur();
				else if (x=='8')
					restore_cur();
				break;
			case 2:
				for(npar=0;npar<NPAR;npar++)
					par[npar]=0;
				npar=0;
				state=3;
				if ((ques=(c=='?')))
					break;
			case 3:
				if (c==';' && npar<NPAR-1) {
					npar++;
					break;
				} else if (c>='0' && c<='9') {
					par[npar]=10*par[npar]+c-'0';
					break;
				} else state=4;
			case 4:
				state=0;
				switch(c) {
					case 'G': case '`':
						if (par[0]) par[0]--;
						gotoxy(par[0],y);
						break;
					case 'A':
						if (!par[0]) par[0]++;
						gotoxy(x,y-par[0]);
						break;
					case 'B': case 'e':
						if (!par[0]) par[0]++;
						gotoxy(x,y+par[0]);
						break;
					case 'C': case 'a':
						if (!par[0]) par[0]++;
						gotoxy(x+par[0],y);
						break;
					case 'D':
						if (!par[0]) par[0]++;
						gotoxy(x-par[0],y);
						break;
					case 'E':
						if (!par[0]) par[0]++;
						gotoxy(0,y+par[0]);
						break;
					case 'F':
						if (!par[0]) par[0]++;
						gotoxy(0,y-par[0]);
						break;
					case 'd':
						if (par[0]) par[0]--;
						gotoxy(x,par[0]);
						break;
					case 'H': case 'f':
						if (par[0]) par[0]--;
						if (par[1]) par[1]--;
						gotoxy(par[1],par[0]);
						break;
					case 'J':
						csi_J(par[0]);
						break;
					case 'K':
						csi_K(par[0]);
						break;
					case 'L':
						csi_L(par[0]);
						break;
					case 'M':
						csi_M(par[0]);
						break;
					case 'P':
						csi_P(par[0]);
						break;
					case '@':
						csi_at(par[0]);
						break;
					case 'm':
						csi_m();
						break;
					case 'r':
						if (par[0]) par[0]--;
						if (!par[1]) par[1]=lines;
						if (par[0] < par[1] &&
						    par[1] <= lines) {
							top=par[0];
							bottom=par[1];
						}
						break;
					case 's':
						save_cur();
						break;
					case 'u':
						restore_cur();
						break;
				}
		}
	}
	set_cursor();
}

/*
 *  void con_init(void);
 *
 * This routine initalizes console interrupts, and does nothing
 * else. If you want the screen to clear, call tty_write with
 * the appropriate escape-sequece.
 */
void con_init(void)
{
	register unsigned char a;

	gotoxy(*(unsigned char *)(0x90000+510),*(unsigned char *)(0x90000+511));
	set_trap_gate(0x21,&keyboard_interrupt);
	outb_p(inb_p(0x21)&0xfd,0x21);
	a=inb_p(0x61);
	outb_p(a|0x80,0x61);
	outb(a,0x61);
}
