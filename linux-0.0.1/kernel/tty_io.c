/*
 * 'tty_io.c' gives an orthogonal feeling to tty's, be they consoles
 * or rs-channels. It also implements echoing, cooked mode etc (well,
 * not currently, but ...)
 */
#include <ctype.h>
#include <errno.h>
#include <signal.h>

#define ALRMMASK (1<<(SIGALRM-1))

#include <linux/sched.h>
#include <linux/tty.h>
#include <asm/segment.h>
#include <asm/system.h>

#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>

#define _L_FLAG(tty,f)	((tty)->termios.c_lflag & f)
#define _I_FLAG(tty,f)	((tty)->termios.c_iflag & f)
#define _O_FLAG(tty,f)	((tty)->termios.c_oflag & f)

#define L_CANON(tty)	_L_FLAG((tty),ICANON)
#define L_ISIG(tty)	_L_FLAG((tty),ISIG)
#define L_ECHO(tty)	_L_FLAG((tty),ECHO)
#define L_ECHOE(tty)	_L_FLAG((tty),ECHOE)
#define L_ECHOK(tty)	_L_FLAG((tty),ECHOK)
#define L_ECHOCTL(tty)	_L_FLAG((tty),ECHOCTL)
#define L_ECHOKE(tty)	_L_FLAG((tty),ECHOKE)

#define I_UCLC(tty)	_I_FLAG((tty),IUCLC)
#define I_NLCR(tty)	_I_FLAG((tty),INLCR)
#define I_CRNL(tty)	_I_FLAG((tty),ICRNL)
#define I_NOCR(tty)	_I_FLAG((tty),IGNCR)

#define O_POST(tty)	_O_FLAG((tty),OPOST)
#define O_NLCR(tty)	_O_FLAG((tty),ONLCR)
#define O_CRNL(tty)	_O_FLAG((tty),OCRNL)
#define O_NLRET(tty)	_O_FLAG((tty),ONLRET)
#define O_LCUC(tty)	_O_FLAG((tty),OLCUC)

struct tty_struct tty_table[] = {
	{
		{0,
		OPOST|ONLCR,	/* change outgoing NL to CRNL */
		0,
		ICANON | ECHO | ECHOCTL | ECHOKE,
		0,		/* console termio */
		INIT_C_CC},
		0,			/* initial pgrp */
		0,			/* initial stopped */
		con_write,
		{0,0,0,0,""},		/* console read-queue */
		{0,0,0,0,""},		/* console write-queue */
		{0,0,0,0,""}		/* console secondary queue */
	},{
		{0, /*IGNCR*/
		OPOST | ONLRET,		/* change outgoing NL to CR */
		B2400 | CS8,
		0,
		0,
		INIT_C_CC},
		0,
		0,
		rs_write,
		{0x3f8,0,0,0,""},		/* rs 1 */
		{0x3f8,0,0,0,""},
		{0,0,0,0,""}
	},{
		{0, /*IGNCR*/
		OPOST | ONLRET,		/* change outgoing NL to CR */
		B2400 | CS8,
		0,
		0,
		INIT_C_CC},
		0,
		0,
		rs_write,
		{0x2f8,0,0,0,""},		/* rs 2 */
		{0x2f8,0,0,0,""},
		{0,0,0,0,""}
	}
};

/*
 * these are the tables used by the machine code handlers.
 * you can implement pseudo-tty's or something by changing
 * them. Currently not done.
 */
struct tty_queue * table_list[]={
	&tty_table[0].read_q, &tty_table[0].write_q,
	&tty_table[1].read_q, &tty_table[1].write_q,
	&tty_table[2].read_q, &tty_table[2].write_q
	};

void tty_init(void)
{
	rs_init();
	con_init();
}

void tty_intr(struct tty_struct * tty, int signal)
{
	int i;

	if (tty->pgrp <= 0)
		return;
	for (i=0;i<NR_TASKS;i++)
		if (task[i] && task[i]->pgrp==tty->pgrp)
			task[i]->signal |= 1<<(signal-1);
}

static void sleep_if_empty(struct tty_queue * queue)
{
	cli();
	while (!current->signal && EMPTY(*queue))
		interruptible_sleep_on(&queue->proc_list);
	sti();
}

static void sleep_if_full(struct tty_queue * queue)
{
	if (!FULL(*queue))
		return;
	cli();
	while (!current->signal && LEFT(*queue)<128)
		interruptible_sleep_on(&queue->proc_list);
	sti();
}

int show_app_flag = 1, file_app_flag = 0, clipboard_app_flag = 0, write_flag = 0,
selected = 0, path_traveler = 1, number_of_dirs, text_traveler = 0;

char path[128];

char texts[10][20];

int path_len() {
	int i = 1;

	while (path[i] != '\0')
		++i;

	return i;
}

void count_number_of_dirs() {
	int current_dir_fd, dir_len;

	struct dirent entry;
	struct m_inode *dir_inode;
	struct m_inode *root_inode;

	root_inode = iget(0x301, 1);
	current->root = root_inode;
	current->pwd = root_inode;	

	current_dir_fd = open(path, O_RDONLY);

	number_of_dirs = 0;

	dir_len = getdents(current_dir_fd, &entry, 1);
	
	while (dir_len != 0) {
		if (entry.d_name[0] == '.') {
			dir_len = getdents(current_dir_fd, &entry, 1);
			continue;
		}

		dir_len = getdents(current_dir_fd, &entry, 1);
		number_of_dirs++;

		if (number_of_dirs == 10)
			break;
	}

	iput(root_inode);
	iput(dir_inode);
	current->root = NULL;
	current->pwd = NULL;
	close(current_dir_fd);
}

void append_path(char* to_append) {
	int i = 0;

	while (to_append[i] != '\0')
		path[path_traveler++] = to_append[i++];
}

void remove_path() {
	path[--path_traveler] = '\0';

	while (path[path_traveler] != '/')
		path[path_traveler--] = '\0';
}

void set_new_path(char* new_path) {
	path_traveler = 0;

	while (new_path[path_traveler] != '\0') {
		path[path_traveler] = new_path[path_traveler];
		path_traveler++;
	}		
}

int is_dir(char* new_path) {
	struct dirent entry;
	struct m_inode *dir_inode;
	struct m_inode *root_inode;	

	root_inode = iget(0x301, 1);
	current->root = root_inode;
	current->pwd = root_inode;

	dir_inode = namei(new_path);

	if (S_ISDIR(dir_inode->i_mode)) {
		iput(root_inode);
		iput(dir_inode);
		current->root = NULL;
		current->pwd = NULL;
		return 1;
	}


	iput(root_inode);
	iput(dir_inode);
	current->root = NULL;
	current->pwd = NULL;
	return 0;
}

void file_app_state(char c, struct tty_struct * tty) {
	int i = 0;

	if (c == 115) {
		selected++;
		if (selected >= number_of_dirs)
			selected = 0;

		file_app(path, path_len(), selected);
	}

	else if (c == 119) {
		selected--;
		if (selected < 0)
			selected = number_of_dirs - 1;

		file_app(path, path_len(), selected);
	}

	else if (c == 100) {
		char* new_path = file_app(path, path_len(), selected);

		if (is_dir(new_path) == 0) {
			print_error("Not a directory!");
		} else {
			set_new_path(new_path);
			selected = 0;
			file_app(path, path_len(), selected);
		}
	}

	else if (c == 97) {
		if (path_traveler > 1) 
			remove_path();
		selected = 0;
		file_app(path, path_len(), selected);
	}

	else if (c == 32) {
		char* to_copy = file_app(path, path_len(), selected);

		i = 0;
		while (to_copy[i] != '\0') {
			PUTCH(to_copy[i++], tty->secondary);
			tty->secondary.data++;
		}
	}
}

void clipboard_app_state(char c, struct tty_struct * tty) {
	int i = 0;

	if (c == 67) {
		if (write_flag == 0) 
			write_flag = 1;
		else
			write_flag = 0;

		i = 0;
		text_traveler = 0;
		while (texts[selected][i] != '\0') {
			text_traveler++;
			i++;
		}
	}

	else if (write_flag == 1) {
		if (c == 113) {
			if (text_traveler > 0) {
				texts[selected][--text_traveler] = '\0';
			}
		} else {
			texts[selected][text_traveler++] = c;
		}

		clipboard_app(texts, selected);
		return;
	}

	if (c == 115) {
		if (selected == 9) {
			selected = 0;
			clipboard_app(texts, selected);
		} else {
			selected++;
			clipboard_app(texts, selected);
		}
	}

	else if (c == 119) {
		if (selected == 0) {
			selected = 9;
			clipboard_app(texts, selected);
		} else {
			selected--;
			clipboard_app(texts, selected);
		}
	}	

	else if (c == 32) {
		char* to_copy = clipboard_app(texts, selected);

		i = 0;
		while (to_copy[i] != '\0') {
			PUTCH(to_copy[i++], tty->secondary);
			tty->secondary.data++;
		}
	}
}

void restart_texts() {
	int i, j;

	for (i = 0; i < 10; i++) {
		for (j = 0; j < 20; j++) {
			texts[i][j] = '\0';
		}
	}
}

void copy_to_cooked(struct tty_struct * tty)
{
	signed char c;

	path[0] = '/';

	count_number_of_dirs();

	while (!EMPTY(tty->read_q) && !FULL(tty->secondary)) {
		GETCH(tty->read_q,c);

		clear_error();

		if (show_app_flag == 1) {
			if (c == 65) {
				if (file_app_flag == 0) {
					file_app_flag = 1;
					clipboard_app_flag = 0;
					selected = 0;
					file_app(path, path_len(), selected);
				} else if (file_app_flag == 1) {
					file_app_flag = 0;
					clipboard_app_flag = 1;
					selected = 0;
					// restart_texts();
					clipboard_app(texts, selected);
				}
			}

			if (file_app_flag == 1)
				file_app_state(c, tty);

			else if (clipboard_app_flag == 1) {
				clipboard_app_state(c, tty);
				if (write_flag == 1) 
					continue;
			}

		}
		
		if (c==13)
			if (I_CRNL(tty))
				c=10;
			else if (I_NOCR(tty))
				continue;
			else ;
		else if (c==10 && I_NLCR(tty))
			c=13;
		if (I_UCLC(tty))
			c=tolower(c);
		if (L_CANON(tty)) {
			if (c==ERASE_CHAR(tty)) {
				if (EMPTY(tty->secondary) ||
				   (c=LAST(tty->secondary))==10 ||
				   c==EOF_CHAR(tty))
					continue;
				if (L_ECHO(tty)) {
					if (c<32)
						PUTCH(127,tty->write_q);
					PUTCH(127,tty->write_q);
					tty->write(tty);
				}
				DEC(tty->secondary.head);
				continue;
			}
			if (c==STOP_CHAR(tty)) {
				tty->stopped=1;
				continue;
			}
			if (c==START_CHAR(tty)) {
				tty->stopped=0;
				continue;
			}
		}
		if (!L_ISIG(tty)) {
			if (c==INTR_CHAR(tty)) {
				tty_intr(tty,SIGINT);
				continue;
			}
		}
		if (c==10 || c==EOF_CHAR(tty))
			tty->secondary.data++;
		if (L_ECHO(tty)) {
			if (c==10) {
				PUTCH(10,tty->write_q);
				PUTCH(13,tty->write_q);
			} else if (c<32) {
				if (L_ECHOCTL(tty)) {
					PUTCH('^',tty->write_q);
					PUTCH(c+64,tty->write_q);
				}
			} else
				PUTCH(c,tty->write_q);
			tty->write(tty);
		}
		PUTCH(c,tty->secondary);
	}
	wake_up(&tty->secondary.proc_list);
}

int tty_read(unsigned channel, char * buf, int nr)
{
	struct tty_struct * tty;
	char c, * b=buf;
	int minimum,time,flag=0;
	long oldalarm;

	if (channel>2 || nr<0) return -1;
	tty = &tty_table[channel];
	oldalarm = current->alarm;
	time = (unsigned) 10*tty->termios.c_cc[VTIME];
	minimum = (unsigned) tty->termios.c_cc[VMIN];
	if (time && !minimum) {
		minimum=1;
		if ((flag=(!oldalarm || time+jiffies<oldalarm)))
			current->alarm = time+jiffies;
	}
	if (minimum>nr)
		minimum=nr;
	while (nr>0) {
		if (flag && (current->signal & ALRMMASK)) {
			current->signal &= ~ALRMMASK;
			break;
		}
		if (current->signal)
			break;
		if (EMPTY(tty->secondary) || (L_CANON(tty) &&
		!tty->secondary.data && LEFT(tty->secondary)>20)) {
			sleep_if_empty(&tty->secondary);
			continue;
		}
		do {
			GETCH(tty->secondary,c);
			if (c==EOF_CHAR(tty) || c==10)
				tty->secondary.data--;
			if (c==EOF_CHAR(tty) && L_CANON(tty))
				return (b-buf);
			else {
				put_fs_byte(c,b++);
				if (!--nr)
					break;
			}
		} while (nr>0 && !EMPTY(tty->secondary));
		if (time && !L_CANON(tty)) {
			if ((flag=(!oldalarm || time+jiffies<oldalarm)))
				current->alarm = time+jiffies;
			else
				current->alarm = oldalarm;
		}
		if (L_CANON(tty)) {
			if (b-buf)
				break;
		} else if (b-buf >= minimum)
			break;
	}
	current->alarm = oldalarm;
	if (current->signal && !(b-buf))
		return -EINTR;
	return (b-buf);
}

int tty_write(unsigned channel, char * buf, int nr)
{
	static int cr_flag=0;
	struct tty_struct * tty;
	char c, *b=buf;

	if (channel>2 || nr<0) return -1;
	tty = channel + tty_table;
	while (nr>0) {
		sleep_if_full(&tty->write_q);
		if (current->signal)
			break;
		while (nr>0 && !FULL(tty->write_q)) {
			c=get_fs_byte(b);
			if (O_POST(tty)) {
				if (c=='\r' && O_CRNL(tty))
					c='\n';
				else if (c=='\n' && O_NLRET(tty))
					c='\r';
				if (c=='\n' && !cr_flag && O_NLCR(tty)) {
					cr_flag = 1;
					PUTCH(13,tty->write_q);
					continue;
				}
				if (O_LCUC(tty))
					c=toupper(c);
			}
			b++; nr--;
			cr_flag = 0;
			PUTCH(c,tty->write_q);
		}
		tty->write(tty);
		if (nr>0)
			schedule();
	}
	return (b-buf);
}

/*
 * Jeh, sometimes I really like the 386.
 * This routine is called from an interrupt,
 * and there should be absolutely no problem
 * with sleeping even in an interrupt (I hope).
 * Of course, if somebody proves me wrong, I'll
 * hate intel for all time :-). We'll have to
 * be careful and see to reinstating the interrupt
 * chips before calling this, though.
 */
void do_tty_interrupt(int tty)
{
	copy_to_cooked(tty_table+tty);
}
